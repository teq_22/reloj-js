function obtenerHora(){
    let fecha = new Date();
   // console.log(fecha.getHours())
    //console.log(fecha.getMinutes())
    //console.log(fecha.getSeconds())
   // console.log(fecha.getDate());
   // console.log(fecha.getDay());

    //traer etiquetas html
    let pDiaSemana = document.getElementById('diaSemana');
        pDia = document.getElementById('dia');
        pMes= document.getElementById('mes');
        pAño = document.getElementById('año');
        pHoras = document.getElementById('horas');
        pMinutos = document.getElementById('minutos');
        pSegundos = document.getElementById('segundos');
        pAmPm = document.getElementById('formato');

    let diaSemana = ["Domingo","Lunes","martes","miercoles","jueves","viernes","sabado"]

    let meses = ["enero", "febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre"]

    //asignar valor 
    pDiaSemana.innerText = diaSemana[fecha.getDay()];
    pDia.innerText = fecha.getDate();
    pMes.innerText = meses[fecha.getMonth()];
    pAño.innerText = fecha.getFullYear();
    pHoras.innerText = fecha.getHours();

    if(fecha.getHours() >12){
        if((fecha.getHours() - 12) <10){
            
            pHoras.innerText = "0" + (fecha.getHours() - 12);
        }else{
            pHoras.innerText = fecha.getHours() - 12;
        }
    }
    if(fecha.getHours() >=12){
        pAmPm.innerText = "PM"
    }else{
        pAmPm.innerText = "AM"
    }

    
    

    if(fecha.getSeconds() < 10){
        pSegundos.innerText = "0" + fecha.getSeconds();
    }else{
        pSegundos.innerText = fecha.getSeconds();
    }

    if(fecha.getMinutes() < 10){
        pMinutos.innerText = "0" + fecha.getMinutes();
    }else{
        pMinutos.innerText = fecha.getMinutes();
    }


}
window.setInterval(obtenerHora,1000);